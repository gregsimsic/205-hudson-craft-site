<?php
/*
 *
 * DEPLOY RECIPE -- no git, non-atomic
 *
 *
 */

namespace Deployer;

require 'recipe/common.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/db.php';
require 'vendor/gregsimsic/deployer-recipes/recipes/sync.php';
//require 'vendor/gregsimsic/deployer-recipes/recipes/oper.php';
require 'vendor/gregsimsic/deployer-recipes/lib/Utils.php';

/**
 *  CONFIG
 *
 */

// read hosts from config
inventory('deploy.yml');

set('application', '205 Hudson');

set('default_stage', 'stage');

// The list of directories given as options to the sync task -- no leading or trailing slashes
set('sync_items', [
    'web/media',
    'web/media/submissions',
    'web/assets',
    'templates'
]);

/**
 *  DEPLOY TASK
 *
 */
desc('Basic Deploy Task (No Git, Not Atomic)');
task('deploy', function () {
    writeln("There is not automatic deploy task for this site.");
});
