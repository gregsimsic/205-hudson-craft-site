## Site for Hunter MFA Program at 205 Hudson

Site JS and CSS is built on Zurb Foundation 6

created in and using Node 8

## Frontend 



`gulp`

Build for production:

`gulp --production`



## Deployer

Setup remote server credentials in deploy.yml. The default stage is 'stage'

    dep db:pull <stage>
    dep db:push <stage>
    dep sync:up <stage>
    dep sync:down <stage>
    dep sync:all <stage>
    dep sync:remotes

Sync command provides prompts
