module.exports = {
    filenameHashing: false,
    // to serve files while working on a project from a different server
    devServer: {
        disableHostCheck: true, // got rid of console errors
        hot: true,
        port: 8092,
        // host: 'mfa205hudson3.lcl',
        // sockHost: 'http://localhost',
        // sockPort: 8080,
        headers: {
            'Access-Control-Allow-Origin': '*' // CORS
        }
    }
}
