'use strict';

import $ from 'jquery';
import { Plugin } from 'foundation-sites/js/foundation.plugin';

/**
 * SetHeight module.
 */
class SetHeight extends Plugin {

  _setup(element, options) {
    this.$element = element;
    this.options = $.extend({}, SetHeight.defaults, element.data(), options);
    this.className = 'SetHeight';

    this._init();
  }

  _init() {

    console.log('set height');

    /*
    TODO: The target is ....
     */
    this.$target = $('.' + this.options.target);

    $(window).on('resize.zf.trigger', () => {
      this._setHeight();
    });

    this._setHeight();

  }

  _setHeight() {

    // set height as ratio of width of the target element
    let height = this.$target.width() * this.options.ratio;

    // constrain to max height
    height = (height < this.options.maxHeight) ? height : this.options.maxHeight;

    this.$target.css('height', height);

    if (Foundation.MediaQuery.atLeast('large')) {
      this.$element.css('height', height);
    } else {
      this.$element.css('height','inherit');
    }
  }

  /**
   * Destroys the instance of SetHeight on the element.
   * @function
   */
  _destroy() {
    this.$element.off('.zf.console');
  }

}

SetHeight.defaults = {
  maxHeight: 600,
  ratio: 0.75 // 4:3
};

export {SetHeight};
