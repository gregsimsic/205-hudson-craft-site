'use strict';

import $ from 'jquery';
import { Plugin } from 'foundation-sites/js/foundation.plugin';

/**
 * ThesisSlideshow module.
 */
class ThesisSlideshow extends Plugin {

  _setup(element, options) {
    this.$element = element;
    this.options = $.extend({}, ThesisSlideshow.defaults, element.data(), options);
    this.className = 'ThesisSlideshow';

    this._init();
  }

  _init() {

    console.log('thesis slideshow init');

    // load vimeo iframe library
    $.getScript("http://a.vimeocdn.com/js/froogaloop2.min.js");

    this.$thumbs = $('.m-studentThesisSlideshow__thumbs');
    this.$caption = $('.m-studentThesisSlideshow__featureCaptions');
    this.$featureWrapper = $('.m-studentThesisSlideshow__featureWrapper'),

    // vimeo player
    this.player = undefined;

    this._create();

  }

  _initSlide(currentSlide) {

    let _self = this;

    // stop any playing videos
    if(_self.player) {
      _self.player.api('pause');
    }

    let $currentSlide = this.$element.find('.slick-slide:not(.slick-cloned)').eq(currentSlide);

    // set caption
    _self.$caption.html( $currentSlide.find('.m-studentThesisSlideshow__caption').html() );

    // highlight thumb
    _self.$thumbs.find('li').removeClass('is-selected').eq(currentSlide).addClass('is-selected');

    // set highlight on 2nd set of dots
    $('.m-studentThesisSlideshow__dots--mobile').find('li').removeClass('slick-active').eq(currentSlide).addClass('slick-active');

    // if video
    if( $currentSlide.hasClass('is-video')) {

      $('.m-studentThesisSlideshow__vidOverlay').click( function() {
        let iframe = $currentSlide.find('iframe').get(0);
        _self.player = $f(iframe);
        _self.player.addEvent('ready', function() {
          _self.player.api('play');
        });
        $currentSlide.find('.m-studentThesisSlideshow__vidOverlay').hide();
      });
    }

  }

  _create() {

    let _self = this;

    // init slick
    this.$element.slick({
      autoplay: false,
      swipeToSlide: true,
      swipe: true,
      infinite: true,
      arrows: false,
      speed: 0,
      dots: true,
      appendDots: '.m-studentThesisSlideshow__dots'
    });

    this.$element.on('afterChange', (event, slick, currentSlide) => {

      _self._initSlide(currentSlide);

    });

    this.$element.on('beforeChange', (event, slick, currentSlide, nextSlide) => {

      let $currentSlide = $(slick.$slides[currentSlide]);

      // turn video overlay back on
      $currentSlide.find('.m-studentThesisSlideshow__vidOverlay').show();

    });

    // init setup
    _self._initSlide(0);

    // nav events
    $('.m-studentThesisSlideshow__featureNavPrevious').click( function() {
      _self.$element.slick('slickPrev');
    });

    $('.m-studentThesisSlideshow__featureNavNext').click( function() {
      _self.$element.slick('slickNext');
    });

    // thumbs click to advance slides
    _self.$thumbs.find('li').click( function() {
      let index = $(this).index();
      _self.$element.slick('slickGoTo', index);
      _self.$featureWrapper.removeClass('s-thumbs-on');
    });
    // show/hide slides
    $('.m-studentThesisSlideshow__viewSwitcher--slides').click( function(e) {
      e.preventDefault();
      _self.$featureWrapper.removeClass('s-thumbs-on');
    });
    $('.m-studentThesisSlideshow__viewSwitcher--thumbs').click( function(e) {
      e.preventDefault();
      _self.$featureWrapper.addClass('s-thumbs-on');
    });

  }

  /**
   * Destroys the instance of TnsForm on the element.
   * @function
   */
  _destroy() {
    this.$element.off('.zf.console');
  }

}

ThesisSlideshow.defaults = {
  key: 'value'
};

export {ThesisSlideshow};
