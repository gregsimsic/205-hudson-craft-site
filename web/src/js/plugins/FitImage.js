'use strict';

import $ from 'jquery';
import { Plugin } from 'foundation-sites/js/foundation.plugin';

/**
 * FitImage module.
 */
class FitImage extends Plugin {

  _setup(element, options) {
    this.$element = element;
    this.options = $.extend({}, FitImage.defaults, element.data(), options);
    this.className = 'FitImage';

    this._init();
  }

  _init() {

    this.$element.get(0).addEventListener("load", () => {
      this._setDimensions();
    });

    // F's debounced resize
    $(window).on('resize.zf.trigger', () => {
      this._setDimensions();
    });

    this._setDimensions();
  }

  _setDimensions() {

    // calc image ratio
    var imgRatio = this.$element.get(0).naturalWidth / this.$element.get(0).naturalHeight;

    // calc container ratio
    var $container = this.$element.parent();
    var containerRatio = $container.outerWidth() / $container.outerHeight();

    // set width & height
    if(imgRatio > containerRatio) {
      this.$element.removeClass('vertical-crop').addClass('horizontal-crop');

      var position = this.$element.data("focusx"),
        styles;

      if (position <= 50) {
        styles = {
          left: position + "%",
          marginLeft: this.$element[0].clientWidth * (position / -100) + "px"
        };
      } else {
        position = 100 - position;
        styles = {
          right: position + "%",
          marginRight: this.$element[0].clientWidth * (position / -100) + "px"
        };
      }
      this.$element.removeAttr('style').css(styles);

    } else {
      this.$element.removeClass('horizontal-crop').addClass('vertical-crop');

      var position = this.$element.data("focusy");

      this.$element.removeAttr('style').css({
        top: position + "%",
        marginTop: this.$element[0].clientHeight * (position / -100) + "px"
      });

    }

  };

  /**
   * Destroys the instance of TnsForm on the element.
   * @function
   */
  _destroy() {
    this.$element.off('.zf.console');
  }

}

FitImage.defaults = {
  key: 'value'
};

export {FitImage};
