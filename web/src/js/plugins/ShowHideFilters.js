'use strict';

import $ from 'jquery';
import { Plugin } from 'foundation-sites/js/foundation.plugin';

/**
 * ShowHideFilters module.
 */
class ShowHideFilters extends Plugin {

  _setup(element, options) {
    this.$element = element;
    this.options = $.extend({}, ShowHideFilters.defaults, element.data(), options);
    this.className = 'ShowHideFilters';

    this._init();
  }

  _init() {

    let that = this;

    // show/hide
    $(".m-filters__toggler").click(function() {
      that.$element.toggleClass('s-open');
    });

    let $items = $('.' + this.options.selectorClass);

    $('.' + that.options.filterClass).find('a').click(function(e) {

      e.preventDefault();

      let $this = $(this).closest('li');

      // if an 'all' filter was already and clicked do nothing
      if($this.hasClass('active') && $this.hasClass('all')) {
        return false;
      }

      // turn off other filters in this group
      $this.closest('ul').children('.active').not($this).removeClass('active');

      // a filter in this group is being turned off, then turn on the 'all' filter
      if($this.hasClass('active')) {
        $this.closest('ul').children('.all').addClass('active');
      }

      $this.toggleClass('active');

      // get all filters with class 'active'
      let activeFilters = $('.' + that.options.filterClass + '.active').map(function() {
        return $(this).data('filter') === '' ? null : $(this).data('filter');
      }).get().join('.');

      // if there are no active filters, then set the filter to the selector class which is on all items
      activeFilters = (activeFilters === '') ? that.options.selectorClass : activeFilters;

      $items.hide().filter('.' + activeFilters).show();

    });

  }

  /**
   * Destroys the instance of  on the element.
   * @function
   */
  _destroy() {
    this.$element.off('.zf.console');
  }

}

ShowHideFilters.defaults = {
  filterClass: 'm-filters__filter'
};

export {ShowHideFilters};
