'use strict';

import $ from 'jquery';
import { Plugin } from 'foundation-sites/js/foundation.plugin';

/**
 * MatrixSlideshow module.
 */
class MatrixSlideshow extends Plugin {

  _setup(element, options) {
    this.$element = element;
    this.options = $.extend({}, MatrixSlideshow.defaults, element.data(), options);
    this.className = '';

    this._init();
  }

  _init() {

    console.log('gotcha');

    let prevArrowHtml = "<div class='slick-arrow slick-prev'><div class='arrow-box'><img src='' /></div></div>";
    let nextArrowHtml = "<div class='slick-arrow slick-next'><div class='arrow-box'><img src='' /></div></div>";

    // matrix slideshow
    this.$element.slick({
      autoplay: false,
      autoplaySpeed: 5000,
      arrows: true,
      accessibility: true,
      prevArrow: prevArrowHtml,
      nextArrow: nextArrowHtml
    });

  }

  /**
   * Destroys the instance of MatrixSlideshow on the element.
   * @function
   */
  _destroy() {
    this.$element.off('.zf.console');
  }

}

MatrixSlideshow.defaults = {
  key: 'value'
};

export {MatrixSlideshow};
