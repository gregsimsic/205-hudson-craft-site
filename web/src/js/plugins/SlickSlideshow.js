'use strict';

import $ from 'jquery';
import { Plugin } from 'foundation-sites/js/foundation.plugin';

/**
 * SlickSlideshow module.
 */
class SlickSlideshow extends Plugin {

  _setup(element, options) {
    this.$element = element;
    this.options = $.extend({}, SlickSlideshow.defaults, options);
    this.className = 'SlickSlideshow';

    this._init();
  }

  _init() {

    console.log('init slick');

    // get json options
    let inlineOptions = JSON.parse(this.$element.data('settings'));

    let slickOptions = $.extend({}, this.options.slickDefaults, inlineOptions);

    // add class to first visible slide in order to style it
    this.$element.on('afterChange', function(event, slick, currentSlide){
      slick.$slides.removeClass('leftMostSlide');
      slick.$slides.each( function (i) {
        var left = $(this).offset().left;
        if ( left > -1 && left < 10 ) {
          $(this).addClass('leftMostSlide');
        }
      });
    }).on('init', function(event, slick){
      slick.$slides.eq(0).addClass('leftMostSlide');
    });

    this.$element.slick( slickOptions );

  }

  /**
   * Destroys the instance of SlickSlideshow on the element.
   * @function
   */
  _destroy() {
    this.$element.off('.zf.console');
  }

}

SlickSlideshow.defaults = {
  prevArrowHtml: "<div class='slick-arrow slick-prev'><div class='arrow-box'><img src='' /></div></div>",
  nextArrowHtml: "<div class='slick-arrow slick-next'><div class='arrow-box'><img src='' /></div></div>",
  slickDefaults: {
    autoplay: false,
    autoplaySpeed: 5000,
    arrows: true,
    accessibility: true
  }
};

export {SlickSlideshow};
