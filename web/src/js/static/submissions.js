// Submission Form
Vue.component('v-select', VueSelect.VueSelect);

new Vue({
    el: '#submissions-form',
    delimiters: ['{(', ')}'],
    data: {
        submission: {
            title: '',
            sectionUid: '6f211ac6-0ffc-4159-b9d4-cdbcb556716c', // Submissions
            enabled: 0,
            fields: {
                descriptionTextarea: '',
                submissionType: -1,
                email: '',
                submissionUrl: '',
                exhibitionStartDate: '',
                exhibitionEndDate: '',
                startTime: '',
                person: [],
                submissionsMedia: null,
                submissionsLocation: '',
                curator: ''
            }
        },

        titleLabel: 'Title',
        previewType: 'Type',

        action: 'guest-entries/save',

        maxFileSizeInBytes: 1000000,
        imageUploadSrc: null,
        placeholder: 'Start Typing a Name',
        peopleOptions: [],

        firstSubmit: false,
        saving: false,
        success: false,
        errors: {},
        generalError: false

    },
    computed: {
        startDateLabel() {
            return this.isExhibition ? 'Start Date' : 'Date';
        },
        imageUploadButtonText() {
            return this.submission.fields.submissionsMedia ? 'Replace the Image' : 'Attach an Image';
        },
        isEventOrExhibition() {
            return ["27307","27303"].indexOf(this.submission.fields.submissionType) != -1;
        },
        isExhibition() {
            return this.submission.fields.submissionType === "27303";
        },
        isEvent() {
            return this.submission.fields.submissionType === "27307";
        },
        formattedStartTime() {
            if (this.submission.fields.exhibitionStartDate && this.submission.fields.startTime) {
                return moment(this.submission.fields.exhibitionStartDate + ' ' + this.submission.fields.startTime).format("h:mm A");
            } else {
                return '<span class="incomplete">Start Time</span>'
            }
        },
        formattedStartDate() {
            const format = this.isExhibition ? "MMMM D" : "MMMM D, YYYY";
            if (this.submission.fields.exhibitionStartDate) {
                return moment(this.submission.fields.exhibitionStartDate).format(format);
            } else {
                return '<span class="incomplete">Start Date</span>'
            }
        },
        formattedEndDate() {
            if (this.submission.fields.exhibitionEndDate) {
                return moment(this.submission.fields.exhibitionEndDate).format("MMMM D, YYYY");
            } else {
                return '<span class="incomplete">End Date</span>'
            }
        },
        previewDescription() {
            let description = '<span class="incomplete">Select a Submission Type to preview the description.</span>';

            if (this.submission.fields.submissionType != -1) {
                const title = this.submission.title || '<span class="incomplete"><em>' + this.titleLabel + '</em></span>';
                if (this.isExhibition) {
                    const location = this.submission.fields.submissionsLocation || '<span class="incomplete">Location</span>';
                    const startDate = this.formattedStartDate;
                    const endDate = this.formattedEndDate;
                    const leader = this.isExhibition ? 'Exhibition at ' : '';
                    const curatedBy = this.submission.fields.curator ? '<span class="slash">/</span> curated by ' + this.submission.fields.curator : '';
                    description = leader + location
                        + ' <span class="slash">/</span> ' + '<em>' + title + '</em>' + curatedBy
                        + ' <span class="slash">/</span> ' + startDate + ' — ' + endDate;
                } else if (this.isEvent) {
                    const location = this.submission.fields.submissionsLocation || '<span class="incomplete">Location</span>';
                    const startDate = this.formattedStartDate;
                    const startTime = this.formattedStartTime;
                    const leader = this.isExhibition ? 'Event at ' : '';
                    const curatedBy = this.submission.fields.curator ? '<span class="slash">/</span> curated by ' + this.submission.fields.curator : '';
                    description = leader + location
                        + ' <span class="slash">/</span> ' + '<em>' + title + '</em>' + curatedBy
                        + ' <span class="slash">/</span> ' + startDate + ' — ' + startTime;
                } else {
                    description = '';
                    const descriptionTextarea = this.submission.fields.descriptionTextarea || '<span class="incomplete">Description</span>';
                    if (this.submission.title) {
                        description = '<em>' + title + '</em>';
                    }
                    if (this.submission.fields.descriptionTextarea) {
                        const separator = description ? ' <span class="slash">/</span> ' : '';
                        description += separator + descriptionTextarea;
                    }
                    if (!description) {
                        description = '<span class="incomplete"><em>' + this.titleLabel + '</em></span> / <span class="incomplete">Description</span>'
                    }
                }
            }

            return description;

        },
        previewName() {
            if (this.submission.fields.person.length > 1) {
                let peopleNames = this.submission.fields.person.map((personId) => {
                    return this.getPersonById(personId).label;
                })
                const oxfordComma = peopleNames.length > 2 ? ',' : '';
                return peopleNames.slice(0, -1).join(', ') + oxfordComma + ' and ' + peopleNames.slice(-1);
            } else {
                const firstPerson = this.getPersonById(this.submission.fields.person[0]);
                if (firstPerson) {
                    return '<span class="personName">' + firstPerson.label + '</span>' + ' <span class="personLabel">'+this.previewLabel+'</span>';
                } else {
                    return '<span class="incomplete">Select one or more persons</span>';
                }
            }
        },
        // previewType() {
        //     return this.submission.fields.submissionType
        // },
        previewLabel() {
            const firstPerson = this.getPersonById(this.submission.fields.person[0]);
            let label = '';
            if (firstPerson) {
                label = firstPerson.personLabel;
            } else {
                label = 'Label'
            }
            return label;
        }
    },
    mounted() {
        this.$el.style.display = 'block';
    },
    created() {
        this.getPeople();
    },
    methods: {
        submitForm(e) {

            this.firstSubmit = true;

            e.preventDefault();
            e.stopImmediatePropagation();

            if(!this.validate()) {
                return false;
            }

            const that = this;

            that.saving = true;

            let formData = new FormData();

            formData.append("action", this.action);
            formData.append("CRAFT_CSRF_TOKEN", this.$el.querySelector('#csrf').value);

            const title = this.submission.title ? this.submission.title : this.submission.fields.descriptionTextarea.substring(0,30) + '...';
            formData.append("title", title);
            formData.append("enabled", this.submission.enabled);
            formData.append("sectionUid", this.submission.sectionUid);

            formData.append("fields[submissionType][]", this.submission.fields.submissionType);
            formData.append("fields[descriptionTextarea]", this.submission.fields.descriptionTextarea);
            formData.append("fields[email]", this.submission.fields.email);
            formData.append("fields[submissionUrl]", this.submission.fields.submissionUrl);
            formData.append("fields[submissionsLocation]", this.submission.fields.submissionsLocation);
            formData.append("fields[curator]", this.submission.fields.curator);
            formData.append("fields[exhibitionStartDate]", this.submission.fields.exhibitionStartDate);
            formData.append("fields[exhibitionEndDate]", this.submission.fields.exhibitionEndDate);
            if (this.submission.fields.startTime) {
                formData.append("fields[startTime]", this.submission.fields.exhibitionStartDate + ' ' + this.submission.fields.startTime);
            }
            formData.append("fields[submissionsMedia][]", this.submission.fields.submissionsMedia);

            this.submission.fields.person.forEach( function(person) {
                formData.append('fields[person][]', person);
            });

            axios({
                method: 'post',
                url: 'index.php?action=' + this.action,
                responseType: 'json',
                data: formData,
                config: {
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'multipart/form-data',
                        'Accept': 'application/json'
                    }
                }
            })
            .then(function (response) {

                that.saving = false;

                if (!response || response.data.errors) {

                    that.generalError = true;

                } else {

                    that.generalError = false;
                    that.success = true;

                }

            })
            .catch(function (response) {
                that.saving = false;
                that.generalError = true;
            });

        },
        validate() {

            this.errors = {};

            if (!this.submission.fields.person.length) {
                this.errors.person = 'Select at least 1 person';
            }

            if (this.submission.fields.submissionType == -1) {
                this.errors.submissionType = 'Select a type';
            }

            if (this.isEventOrExhibition) {

                if (!this.submission.title) {
                    this.errors.title = 'The ' + this.titleLabel + ' is required';
                }

                if (!this.submission.fields.submissionsLocation) {
                    this.errors.submissionsLocation = 'A location is required';
                }

                if (!this.submission.fields.exhibitionStartDate) {
                    this.errors.exhibitionStartDate = 'Select a start date';
                }

                if (this.isEvent) {
                    if (!this.submission.fields.startTime) {
                        this.errors.startTime = 'Select a time';
                    }
                } else {
                    if (!this.submission.fields.exhibitionEndDate) {
                        this.errors.exhibitionEndDate = 'Select an end date';
                    }
                }

            } else {

                if (!this.submission.title && !this.submission.fields.descriptionTextarea) {
                    this.errors.title = 'The ' + this.titleLabel + ' or Description is required';
                    this.errors.descriptionTextarea = 'The ' + this.titleLabel + ' or Description is required';
                }

            }

            if (!this.submission.fields.submissionsMedia) {
                this.errors.submissionsMedia = 'You must provide an image.';
            } else if (this.submission.fields.submissionsMedia.size > this.maxFileSizeInBytes) {
                this.errors.submissionsMedia = 'Please choose an image smaller than 1MB';
            }

            if (this.submission.fields.email && !this.validateEmail(this.submission.fields.email)) {
                this.errors.email = 'This must be a valid email address';
            }

            if (!this.submission.fields.submissionUrl) {
                this.errors.submissionUrl = 'You must provide a link.';
            } else if (!this.validateUrl(this.submission.fields.submissionUrl)) {
                this.errors.submissionUrl = 'The link must be a valid URL';
            }

            return (Object.keys(this.errors).length === 0);

        },
        validateUrl(url) {
            // protocol is required because Craft requires it
            var pattern = new RegExp('^(https?:\\/\\/)'+ // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

            // The Craft URL validation pattern
            // '/^(?:(?:{schemes}:)?\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)?|\/)[^\s]*$/i'

            return !!pattern.test(url);
        },
        validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
        getPeople() {

            const that = this;

            axios({
                method: 'get',
                url: '/people.json',
                responseType: 'json'
            })
            .then(function (response) {

                if (!response || response.data.errors) {

                    that.generalError = true;

                } else {

                    response.data.data.forEach( function(person) {
                        that.peopleOptions.push({
                            label: person.title,
                            value: person.id,
                            sectionId: person.sectionId,
                            personLabel: person.label
                        });
                    })
                }

            })
            .catch(function (response) {
                that.generalError = true;
            });


        },
        getPersonById(id) {
            let foundPerson = null;
            this.peopleOptions.forEach( (person) => {
                if (person.value === id) {
                    foundPerson = person;
                }
            });
            return foundPerson;
        },
        handleChange(e) {
            if(this.firstSubmit) {
                this.validate();
            }
        },
        handleTypeChange(e) {
            this.titleLabel = e.target.selectedOptions[0].dataset.label;
            this.previewType = e.target.selectedOptions[0].label;
            if(this.firstSubmit) {
                this.validate();
            }
        },
        handleImage(e) {

            const that = this;
            const file = e.target.files ? e.target.files[0] : e.dataTransfer.files[0];

            // setting this here force a re-render that will help the error display ??
            this.submission.fields.submissionsMedia = file;

            delete this.errors.submissionsMedia;

            if (file.size > this.maxFileSizeInBytes) {

                this.errors.submissionsMedia = 'Please choose an image smaller than 1MB.';

                this.submission.fields.submissionsMedia = null;

            } else {

                var reader = new FileReader();

                reader.onload = function(event) {
                    that.imageUploadSrc = event.target.result;
                };

                reader.readAsDataURL(file);

            }


        },
        removeImage(e) {

            this.submission.fields.submissionsMedia = null;
            this.imageUploadSrc = null;

        }
    }
})
