// Patch for a Bug in v6.3.1 to fix responsive menus
$(window).on('changed.zf.mediaquery', function() {
  $('.is-dropdown-submenu.invisible').removeClass('invisible');

  // turn menu off when going large
  if (Foundation.MediaQuery.atLeast('large')) {
    $('body').removeClass('s-menu-open');
    $('.is-accordion-submenu').removeClass('is-accordion-submenu');
  } else {
    $('.is-dropdown-submenu').removeClass('is-dropdown-submenu');

    // close all submenus
    $('.m-siteNavigation').find('ul ul').slideUp();
  }

});