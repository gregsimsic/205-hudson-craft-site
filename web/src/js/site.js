import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

// import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/foundation-explicit-pieces';

import 'slick-carousel';

// import and register a custom plugin into the Foundation system
import { MatrixSlideshow } from './plugins/MatrixSlideshow';
Foundation.plugin(MatrixSlideshow, 'MatrixSlideshow');

import { ThesisSlideshow } from './plugins/ThesisSlideshow';
Foundation.plugin(ThesisSlideshow, 'ThesisSlideshow');

import { FitImage } from './plugins/FitImage';
Foundation.plugin(FitImage, 'FitImage');

import { ShowHideFilters } from './plugins/ShowHideFilters';
Foundation.plugin(ShowHideFilters, 'ShowHideFilters');

import { SetHeight } from './plugins/SetHeight';
Foundation.plugin(SetHeight, 'SetHeight');

import { SlickSlideshow } from './plugins/SlickSlideshow';
Foundation.plugin(SlickSlideshow, 'SlickSlideshow');

require('./main');

// inits all plugins
$(document).foundation();

