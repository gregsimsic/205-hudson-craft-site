<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

//define('SITE_URL', '//' . $_SERVER['SERVER_NAME'] . '/');
define('BASEPATH', realpath(CRAFT_BASE_PATH) . '/');

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // queue is run by cron job every 1 minute
        'runQueueAutomatically' => false,

        'loginPath' => 'admin/login',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => false,

        'enableCookieValidation' => false,
        'devMode' => false,
        'useCompressedJs' => false,
        'enableTemplateCaching' => true,
        'cacheDuration' => 'P1D',  // 1 day
        'maxUploadFileSize' => 268435456, // 256MB
//        'imageDriver' => 'imagick', // s/b the default anyway
        'defaultImageQuality' => 100,
        'optimizeImageFilesize' => false, // this is key, it disables Imagicks 'smartresize' resulting in sharper images
        'upscaleImages' => false,
        'aliases' => array(
            'basePath' => BASEPATH . 'web/',
            'baseUrl'  => getenv('DEFAULT_SITE_URL'),
        ),
        'defaultSearchTermOptions' => array(
            'subLeft' => true,
            'subRight' => true,
        ),
        'siteUrl' => getenv('DEFAULT_SITE_URL')

    ],

    // Dev environment settings
    'lcl' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => true,
        'userSessionDuration' => 60 * 60 * 24 * 14, // 2 weeks
    ],

    // Dev environment settings
    'dev' => [
        'devMode' => true,
    ],

    // Staging environment settings
    'staging' => [
        'allowAdminChanges' => true,
    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on production
        'allowAdminChanges' => true,
//        'storeUserIps' => true
    ],
];
