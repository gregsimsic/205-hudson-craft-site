<?php

use craft\elements\Entry;
use modules\huntermfamodule\HuntermfaModule;

return [
    'endpoints' => [
        'people.json' => function() {
            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'section' => 'students,faculty,people',
                    'limit' => null,
                    'orderBy' => 'title',
                    'id' => ['not', 6652, 6747, 6788, 25209] // exclude the student records of T Weaver, J Carreiro, and L C Davis
                ],
                'paginate' => false,
                'cache' => false, // 'PT1M' one minute
                'transformer' => function(Entry $entry) {

                    // set labels
                    switch($entry->sectionId) {
                        case '6':
                            if (HuntermfaModule::getInstance()->huntermfa->studentIsAlumni($entry)) {
                                // specify grad year for alumni
                                $label = 'MFA&nbsp;' . explode('-', $entry->graduationSemester)[0];
                            } else {
                                $label = 'MFA&nbsp;Student';
                            }
                            break;

                        case '16':
                            $label = 'Faculty';
                            break;

                        case '21':
                            $label = $entry->briefTitle;
                            break;

                        default:
                            $label = '';

                    }

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'sectionId' => $entry->sectionId,
                        'label' => $label
                    ];
                },
            ];
        }
    ]
];