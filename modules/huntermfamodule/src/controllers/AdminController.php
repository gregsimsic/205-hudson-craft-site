<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule\controllers;

use Craft;
use craft\helpers\DateTimeHelper;
use craft\web\Controller;
use modules\huntermfamodule\HuntermfaModule;

/**
 * Admin Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your module’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 */
class AdminController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
//    protected $allowAnonymous = ['index', 'update-categories'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        echo date('Y-m-d\TH:i:s\Z', strtotime('+10 minutes'));

        Craft::$app->end();
    }

    /**
     *
     * hootsuite test
     *
     * http://mfa205hudson3.lcl/index.php?action=huntermfa-module/admin/hoot
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionHoot()
    {
        $result = HuntermfaModule::getInstance()->hootsuite->scheduleMessage();

        echo "<pre>";
        print_r($result);
        echo "</pre>";

        Craft::$app->end();

    }

    /**
     *
     * used for miscellaneuos testing
     *
     * http://mfa205hudson3.lcl/index.php?action=huntermfa-module/admin/test
     *
     * @return mixed
     */
    public function actionTest()
    {
//        echo Craft::$app->getImages()->getIsImagick() ? 'true' : 'false';

        $date = '2020-02-03';

        // timeZoneOffset
//        $tz = Craft::$app->getTimeZone();
//        $offset = DateTimeHelper::timeZoneOffset($tz);

//        $ddate = DateTimeHelper::toDateTime($date, false, false);

//        $new_date = date("Y-m-d H:i:s", strtotime('+4 hours', strtotime($date)));

        $dateTime = new \DateTime();
        $timeZone = new \DateTimeZone(Craft::$app->getTimeZone());
        $timeZoneOffset = $timeZone->getOffset($dateTime);
        $timeZoneOffset -= 1200; // 20 minutes
        $interval = \DateInterval::createFromDateString((string)$timeZoneOffset . 'seconds');
        $dateTime->sub($interval);
        $result = $dateTime->format('Y-m-d H:i:s');

        echo "<pre>";
        print_r( [
            'date'=> $date,
            'timezone'=> $timeZone,
            'offset'=> $timeZoneOffset,
            'adjusted' => $result
        ] );
        echo "</pre>";
        die();

    }

    public function actionUpdateCategories()
    {

        die('disabled');

//        $entry = \craft\elements\Entry::find()->id(7561)->one();
//        die($entry->eventLocationSingleCat);
//
//        die($entry->title);

        $report = [];

        $section = 'students';
        $dynamicField = "concentration";
        $singleCatField = 'singleCatConcentration';

        // get all entries of section X
        $entries = \craft\elements\Entry::find()
            ->section($section)
//            ->with($dynamicField)
            ->status(null)
            ->offset(660)
            ->limit(30)
            ->all();

//        echo count($entries);
//        die();

        foreach ($entries as $entry) {

//            echo $entry->$dynamicField . "<br>";

//            if ($entry->id === 5098) continue;

            if ($entry->$dynamicField) {

                $catValue = $entry->$dynamicField;

//                $category = Category::find()
//            ->groupId($source['criteria']['groupId'])
//                    ->id($catValue)
//                    ->one();

//                if ($category) {
//
//                    if ($category->id) {
//                        $catId = $category->id;
//                    } else {
//                        $catId = null;
//                    }

                $entry->setFieldValue($singleCatField, [$catValue]);

                // save entry
                if (!Craft::$app->getElements()->saveElement($entry)) {
                    $report[] = 'Couldn’t save the entry "' . $entry->title;
                } else {
                    $report[] = 'Saved entry "' . $entry->title . '" with cat id: ' . $catValue;
                }

//                } else {
//                    $report[] = 'No category found for "' . $entry->title . '"';
//                }

            }

        }

        echo "<pre>";
        echo count($report) . ' entries attempted';
        print_r($report);
        echo "<pre>";

        die('done');







        // get all categories
        $criteria = craft()->elements->getCriteria(ElementType::Category);
        $criteria->group = 'eventType';
        $eventTypes = $criteria->find();

        $eventTypesArray = [];

        // create array of event types
        foreach ($eventTypes as $eventType) {

//			echo $eventType->title . ': ' . $eventType->id . '<br/>';

            $eventTypesArray[$eventType->title] = $eventType->id;
        }


        // get all events
        $criteria = craft()->elements->getCriteria(ElementType::Entry);
        $criteria->section = 'events';
        $criteria->status = 'any';
        $events = $criteria->find();

        // save each event with category
        foreach ($events as $event)
        {

//			echo $event->eventType . ': ' . $eventTypesArray[$event->eventType] . '<br/>';

            // assign to project
            $event->getContent()->eventCategory = $eventTypesArray[$event->eventType];


            // save asset
            craft()->entries->saveEntry($event);

        }

        die(count($events) . 'events saved');

    }

//    public function actionUpdateEventTypes()
//    {
//
//        die('disabled');
//
//        // get all categories
//        $criteria = craft()->elements->getCriteria(ElementType::Category);
//        $criteria->group = 'eventType';
//        $eventTypes = $criteria->find();
//
//        $eventTypesArray = [];
//
//        // create array of event types
//        foreach ($eventTypes as $eventType) {
//
////			echo $eventType->title . ': ' . $eventType->id . '<br/>';
//
//            $eventTypesArray[$eventType->title] = $eventType->id;
//        }
//
//
//        // get all events
//        $criteria = craft()->elements->getCriteria(ElementType::Entry);
//        $criteria->section = 'events';
//        $criteria->status = 'any';
//        $events = $criteria->find();
//
//        // save each event with category
//        foreach ($events as $event)
//        {
//
////			echo $event->eventType . ': ' . $eventTypesArray[$event->eventType] . '<br/>';
//
//            // assign to project
//            $event->getContent()->eventCategory = $eventTypesArray[$event->eventType];
//
//
//            // save asset
//            craft()->entries->saveEntry($event);
//
//        }
//
//        die(count($events) . 'events saved');
//
//    }

    // move all student images into unique folders
    public function actionMoveImages()
    {
        die('disabled');

        $responses = [];
        $limit = null;

        $parentFolderId = 5; // Students asset folder

        // get entries
        $criteria = craft()->elements->getCriteria(ElementType::Entry);
        $criteria->section = 'students';
        $criteria->limit = $limit;
        $criteria->status = null; // get all whether or not they are enabled
//		$criteria->id = 214; // test one student
        $entries = $criteria->find();

        foreach ($entries as $entry) {

            $slug = $entry->slug;

            // get folder by entry slug
            $query = craft()->db->createCommand();
            $row = $query->select('id,path')
                ->from('assetfolders')
                ->where(array(
                    'name' => $slug
                ))
                ->queryRow();

            // get folder id if it exists or create it
            if($row) {
                $destinationFolderId = $row['id'];

                // TODO: check for actual folder on server, create if doesn't exist ??


            } else {
                // create folder
                $response = craft()->assets->createFolder($parentFolderId, $slug);
                $destinationFolderId = $response->getResponseData()['folderId'];
            }

            $fileIds = [];

            // get inTheStudioImage image
            if($entry->inTheStudioImage->first()) {
                $fileIds[] = $entry->inTheStudioImage[0]->id;
            }

            // get portraitImage image
            if($entry->portraitImage->first()) {
                $fileIds[] = $entry->portraitImage[0]->id;
            }

            // get studioFeatureImage image
            if($entry->studioFeatureImage->first()) {
                $fileIds[] = $entry->studioFeatureImage[0]->id;
            }

            // get studioGallery matrix images
            foreach ( $entry->studioGallery as $slide ) {
                if($slide->image->first()) {
                    $fileIds[] = $slide->image[0]->id;
                }
            }

            // get portfolioGallery matrix images
            foreach ( $entry->portfolioGallery as $slide ) {
                if($slide->image->first()) {
                    $fileIds[] = $slide->image[0]->id;
                }
            }

            // array of actions for each moved file: what to do if the file already exists in the destination
            $actions = array_fill(0, count($fileIds), 'keepBoth');

            // move files
            $response = craft()->assets->moveFiles($fileIds, $destinationFolderId,'', $actions);

            $responses[] = [$slug];
            $responses[] = $fileIds;
            $responses[] = $response->getResponseData();

        }

        echo "<pre>";
        print_r($responses);
        echo "</pre>";
        die();

    }
}
