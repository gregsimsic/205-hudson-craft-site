<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule\services;

use craft\base\Component;
use craft\helpers\StringHelper;
use modules\huntermfamodule\HuntermfaModule;

/**
 * Submissions Service
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 */
class Submissions extends Component
{
    public function test(): string
    {
        return 'test method in Submissions service';
    }

    /**
     *
     * Format description for site
     *
     * @param $submission
     * @return string
     */
    public function formatDescription($submission): string
    {

        $submissionTypeId = $submission->submissionType[0]->id;

        // Exhibitions
        if ($submissionTypeId === '27303') {
            $location = $submission->submissionsLocation;
            $startDate = $submission->exhibitionStartDate ? $submission->exhibitionStartDate->format('F j') : '';
            $endDate = $submission->exhibitionEndDate ? $submission->exhibitionEndDate->format('F j, Y') : '';
            $leader = 'Exhibition at ';
            $curatedBy = $submission->curator ? ' <span class="slash">/</span> curated by ' . $submission->curator : '';
            $description = $leader . $location
                . ' <span class="slash">/</span> ' . '<em>' . $submission->title . '</em>' . $curatedBy
                . ' <span class="slash">/</span> ' . $startDate . ' — ' . $endDate;
        // Events
        } else if ($submissionTypeId === '27307') {
            $location = $submission->submissionsLocation;
            $startDate = $submission->exhibitionStartDate ? $submission->exhibitionStartDate->format('F j, Y') : '';
            $startTime = $submission->startTime ? $submission->startTime->format('g:i A') : '';
            $curatedBy = $submission->curator ? '<span class="slash">/</span> curated by ' . $submission->curator : '';
            $description = $location
                . ' <span class="slash">/</span> ' . '<em>' . $submission->title . '</em>' . $curatedBy
                . ' <span class="slash">/</span> ' . $startDate . ' — ' . $startTime;
        // Other
        } else {
            $description = '';
            if ($submission->title) {
                $description = '<em>' . $submission->title . '</em>';
            }
            if ($submission->descriptionTextarea) {
                $separator = $description ? ' <span class="slash">/</span> ' : '';
                $description .= $separator . $submission->descriptionTextarea;
            }
        }

        return $description;
    }

    /**
     *
     * Format description for social media
     *
     * @param $submission
     * @return string
     */
    public function formatDescriptionSocial($submission): string
    {

        $submissionTypeId = $submission->submissionType[0]->id;

        // Exhibitions
        if ($submissionTypeId === '27303') {
            $location = $submission->submissionsLocation;
            $startDate = $submission->exhibitionStartDate ? $submission->exhibitionStartDate->format('F j') : '';
            $endDate = $submission->exhibitionEndDate ? $submission->exhibitionEndDate->format('F j, Y') : '';
            $leader = 'Exhibition at ';
            $curatedBy = $submission->curator ? ' / curated by ' . $submission->curator : '';
            $description = $leader . $location
                . ' / ' . $submission->title . $curatedBy
                . ' / ' . $startDate . ' — ' . $endDate;
        // Events
        } else if ($submissionTypeId === '27307') {
            $location = $submission->submissionsLocation;
            $startDate = $submission->exhibitionStartDate ? $submission->exhibitionStartDate->format('F j, Y') : '';
            $startTime = $submission->startTime ? $submission->startTime->format('g:i A') : '';
            $curatedBy = $submission->curator ? ' / curated by ' . $submission->curator : '';
            $description = $location
                . ' / ' . $submission->title . $curatedBy
                . ' / ' . $startDate . ' — ' . $startTime;
        // Other
        } else {
            $description = '';
            if ($submission->title) {
                $description = $submission->title;
            }
            if ($submission->descriptionTextarea) {
                $separator = $description ? ' / ' : '';
                $description .= $separator . StringHelper::stripHtml($submission->descriptionTextarea);
            }
        }

        $description .= ' ' . $submission->submissionUrl;

        return $description;
    }

    /**
     *
     * Format name for site
     *
     * @param $persons
     * @return string
     */
    public function formatName($persons): string
    {

        // just in case there's no one, return something
        if (empty($persons)) {
            return '<span class="personName"></span>';
        }

        // multiple
        if (count($persons) > 1) {

            $namesArray = array_map( function($person) {
                return $person->title;
            }, $persons);

            return '<span class="personName">' . $this->oxfordCommaString($namesArray) . '</span>';
        }

        // single
        $firstPerson = $persons[0];
        return '<span class="personName">' . $firstPerson->title . '</span>' . ' <span class="personLabel">'. $this->personLabel($firstPerson) .'</span>';
    }

    /**
     *
     * Format name for social media posts
     *
     * @param $persons
     * @return string
     */
    public function formatNameSocial($persons): string
    {

        // multiple
        if (count($persons) > 1) {

            $namesArray = array_map( function($person) {
                return $person->title;
            }, $persons);

            return $this->oxfordCommaString($namesArray);
        }

        // single
        $firstPerson = $persons[0];

        return $firstPerson->title . ' (' . StringHelper::replace($this->personLabel($firstPerson), '&nbsp;', ' ') . ')';
    }

    /**
     * Given an array of strings, returns a string joined according to oxford comma rules
     *
     * @param array $array
     * @param string $and
     * @return string
     */
    public function oxfordCommaString($array, $and = 'and'): string
    {
        $oxfordComma = count($array) > 2 ? ',' : '';
        return implode(', ', array_slice($array, 0, -1))
            . $oxfordComma . ' ' . $and . ' ' . end($array);
    }

    public function personLabel($person): string
    {

        switch($person->sectionId) {
            // Students
            case '6':
                if (HuntermfaModule::getInstance()->huntermfa->studentIsAlumni($person)) {
                    // specify grad year for alumni
                    $label = 'MFA&nbsp;' . explode('-', $person->graduationSemester)[0];
                } else {
                    $label = 'MFA&nbsp;Student';
                }
                break;

            // Faculty
            case '16':
                $label = 'Faculty';
                break;

            // People
            case '21':
                $label = $person->briefTitle ?: '';
                break;

            // Others
            default:
                $label = '';

        }

        return $label;
    }

}
