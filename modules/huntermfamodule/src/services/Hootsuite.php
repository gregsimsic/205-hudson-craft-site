<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule\services;

use Craft;
use craft\base\Component;
use craft\elements\Entry;
use craft\errors\ElementNotFoundException;
use craft\helpers\Json;

use DateInterval;
use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\RequestOptions;
use http\Exception;

use modules\huntermfamodule\HuntermfaModule;
use modules\huntermfamodule\jobs\SubmitToHootsuiteJob;
use Psr\Http\Message\ResponseInterface;
use putyourlightson\logtofile\LogToFile;
use yii\helpers\VarDumper;
use yii\queue\closure\Job;

/**
 * Hootsuite Service
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 */
class Hootsuite extends Component
{

    // https://platform.hootsuite.com/docs/api/index.html#section/Introduction
    // https://developer.hootsuite.com/docs/getting-started-with-the-rest-api
    // My Apps: https://hootsuite.com/developers/my-apps
    // set up posting to instagram: https://help.hootsuite.com/hc/en-us/articles/204864614-Publish-a-post-to-Instagram

    // This social profile type is not supported by our API at this time

    const CLIENT_ID = '8497f3de-c723-4083-b184-34598c69457e';
    const CLIENT_SECRET = 'kgc6WjpFqP-r';
    const GRANT_TYPE = 'member_app';
    const MEMBER_ID = '22048342';
    const URL_BASE = 'https://platform.hootsuite.com';
    const ACCESS_TOKEN_URL = 'https://platform.hootsuite.com/oauth2/token';
    const AUTH_URL = 'https://platform.hootsuite.com/oauth2/auth';

    const STATUS_UNSENT = 'Unsent';
    const STATUS_PREPPED = 'Prepped';
    const STATUS_QUEUED = 'Queued';
    const STATUS_REQUEUED = 'Re-queued';
    const STATUS_SENT = 'Sent';

    const FACEBOOK_PROFILE = [
        'id' => '131305563'
    ];

    const TWITTER_PROFILE = [
        'id' => '131305716'
    ];

    const INSTAGRAM_PROFILE = [
        'id' => '131445752'  // old: 131305430
    ];

    /**
     * How long Hootsuite will hold the post(s) until posting them to the social media accounts
     *
     */
    const HOOTSUITE_POST_DELAY = 600; // in seconds, 10 minutes = 600

    /**
     * Delay before the job is added to the Craft queue--allowing time for the image to upload to S3
     */
    const QUEUE_DELAY = 300; // in seconds, 10 minutes = 600

    /**
     * Post a submission to Hootsuite
     *
     * @param $submission
     */
    public function scheduleMessage($submission)
    {
        $mediaInfo = $this->getMediaInfo($submission);

        if ($mediaInfo) {
            $this->uploadImage($submission, $mediaInfo);
        }
    }

    /**
     * Post a submission to Hootsuite
     *
     * @param $media_id
     * @return bool
     * @throws Exception
     * @throws \Throwable
     */
    public function postToHootsuite($submissionId)
    {

        LogToFile::info("Submission {$submissionId}: Initiate post to Hootsuite. " . __METHOD__,'submissions');

        $submission = Entry::find()
            ->id($submissionId)
            ->anyStatus()
            ->with('person','submissionType')
            ->one();

        $hootsuiteInfo = Json::decode($submission->hootsuiteInfo);

        $mediaId = $hootsuiteInfo['mediaId'];

        $client = $this->getClient();

        $mediaIdObject = (object) array('id' => $mediaId);

        $requestOptions = [
            'text' => $this->composeMessage($submission),
            'socialProfileIds' => [
                self::TWITTER_PROFILE['id'],
                self::FACEBOOK_PROFILE['id']
            ],
            'scheduledSendTime' => $this->getScheduledPostTime(),
            'media' => [
                $mediaIdObject
            ]
        ];

        try {
            $response = $client->post(
                '/v1/messages',
                [
                    RequestOptions::JSON => $requestOptions
                ]
            );
        } catch (ClientException $e) {

            LogToFile::error("Submission {$submission->id}: Failed to post to Hootsuite. {$e->getMessage()}" . __METHOD__,'submissions');

            // put post back in the queue
            $this->addPostToQueue($submission, true);

            return false;

        }

        if ($response->getStatusCode() === 200) {

            $this->setHootsuiteStatus($submission, self::STATUS_SENT);

            LogToFile::info("Submission {$submission->id}: Posted to Hootsuite. " . __METHOD__,'submissions');

            return true;
        }

        // put post back in the queue
        $this->addPostToQueue($submission, true);

        LogToFile::error("Submission {$submission->id}: Failed to post to Hootsuite. " . __METHOD__,'submissions');

        return false;

    }

    /**
     *
     * Compose message for social media posts
     *
     * @param $submission
     * @return string
     */
    public function composeMessage($submission)
    {
        $description = HuntermfaModule::getInstance()->submissions->formatDescriptionSocial($submission);

        $person = HuntermfaModule::getInstance()->submissions->formatNameSocial($submission->person);

        $message = $person . ' / ' . $description;

        return $message;
    }

    /**
     * Get media upload URL & Id
     *
     * @param $submission
     * @return array|bool
     * @throws ElementNotFoundException
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function getMediaInfo($submission)
    {

        $client = $this->getClient();

        $image = $submission->submissionsMedia->one();

        $imageSizeInBytes = (int)$image->size;
        $mimeType = $image->mimeType;

        try {
            $response = $client->post(
                '/v1/media',
                [
                    RequestOptions::JSON => [
                        'sizeBytes' => $imageSizeInBytes,
                        'mimeType' => $mimeType
                    ]
                ]
            );
        } catch (RequestException $e) {

            LogToFile::error("Submission {$submission->id}: Failed to get media id & url from Hootsuite. {$e->getMessage()}" . __METHOD__,'submissions');

            return false;
        }

        $contents = Json::decode($response->getBody()->getContents());

        $mediaInfo = [
            'mediaId' => $contents['data']['id'],
            'uploadUrl' => $contents['data']['uploadUrl'],
            'sizeBytes' => $imageSizeInBytes,
            'mimeType' => $mimeType
        ];

        $submission->setFieldValue('hootsuiteInfo', Json::encode($mediaInfo));
        $submission->setFieldValue('hootsuiteStatus', self::STATUS_PREPPED);

        $response = Craft::$app->getElements()->saveElement($submission);

        if ($response) {
            LogToFile::info("Submission {$submission->id}: Saved mediaInfo. " . __METHOD__,'submissions');
        } else {
            LogToFile::error("Submission {$submission->id}: Failed to save mediaInfo. {$submission->getErrors()}" . __METHOD__,'submissions');
        }

        return $mediaInfo;

    }

    /**
     * Uploads image file to Hootsuite's AWS S3 account, and returns the media id
     *
     * @param $submission
     * @param $mediaInfo
     * @return bool
     * @throws \Throwable
     */
    public function uploadImage($submission, $mediaInfo): bool
    {

        $image = $submission->submissionsMedia->one();

        $S3Client = new Client();
        $body = $image->getStream();

        try {
            $promise = $S3Client->putAsync(
                $mediaInfo['uploadUrl'],
                [
                    'headers' =>
                        [
                            'Content-Type' => $mediaInfo['mimeType'],
                            'Content-Length' => $mediaInfo['sizeBytes']
                        ],
                    'body' => $body
                ]
            );
        } catch (RequestException $e) {

            LogToFile::error("Submission {$submission->id}: Failed to upload media to S3. {$e->getMessage()}" . __METHOD__,'submissions');

            return false;

        }

        // synchronous wait for S3 response
        $response = $promise->wait();

        // 200 response means the request was accepted, not that the image is ready
        if ($response->getStatusCode() === 200) {

            LogToFile::info("Submission {$submission->id}: Image upload to S3 accepted. " . __METHOD__,'submissions');

            $this->addPostToQueue($submission);

        } else {
            LogToFile::error("Submission {$submission->id}: S3 bad response. " . __METHOD__,'submissions');
        }

        return true;

    }

    /**
     * Queue up a job (with a delay) to allow time for the image to upload to S3
     * A cron job on the server runs every minute to run ./craft queue/run
     *
     * @param $submission
     * @param bool $requeue
     * @throws \Throwable
     */
    public function addPostToQueue($submission, $requeue = false)
    {

        Craft::$app->queue->delay(self::QUEUE_DELAY)->push(
            new SubmitToHootsuiteJob([
                'submissionId' => $submission->id,
            ])
        );

        $status = $requeue ? self::STATUS_REQUEUED : self::STATUS_QUEUED;

        $this->setHootsuiteStatus($submission, $status);

        LogToFile::info("Submission {$submission->id}: Added job to queue. " . __METHOD__,'submissions');

    }

    /**
     * Set value of read-only field on the submission entry
     *
     * @param $key
     * @param $jsonString
     * @return string
     */
    public function extractValueFromJsonString($key, $jsonString): string
    {
        $values = Json::decode($jsonString);

        return $values[$key];
    }

    /**
     * Set value of a key in a Json string
     *
     * @param $key
     * @param $jsonString
     * @return string
     */
    public function setValueOfKeyInJsonString($key, $value, $jsonString): string
    {
        $currentValue = Json::decode($jsonString);
        $currentValue[$key] = $value;
        return Json::encode($currentValue);

    }

    /**
     * Set value of read-only field on the submission entry
     *
     * @param $submission
     * @param $status
     * @return bool
     * @throws \Throwable
     */
    public function setHootsuiteStatus($submission, $newStatus): bool
    {

        $submission->setFieldValue('hootsuiteStatus', $newStatus);

        $response = Craft::$app->getElements()->saveElement($submission);

        return true;

    }

    /**
     * Get social media profiles
     *
     */
    public function getSocialProfiles()
    {
        $client = $this->getClient();

        $response = $client->get( '/v1/socialProfiles' );

        return Json::decode($response->getBody()->getContents());

    }

    /**
     * Returns status of an upload to S3, including status, download url, id, download duration
     *
     * @param $mediaId
     * @return mixed
     */
    public function getMediaUploadStatus($mediaId)
    {
        $client = $this->getClient();

        $response = $client->get( '/v1/media/' . $mediaId );

        return Json::decode($response->getBody()->getContents());

    }

    /**
     * Get basic account about our account, including member id
     *
     */
    public function getMe()
    {
        $client = $this->getClient();

        $response = $client->get( '/v1/me' );

        return Json::decode($response->getBody()->getContents());

    }

    /**
     * Get Guzzle client
     *
     */
    public function getClient(): Client
    {
        if (!$access_token = $this->getToken()) {

            LogToFile::error('Error getting access token from Hootsuite. ' . __METHOD__,'submissions');
        }

        return new Client(
            [
                'base_uri' => self::URL_BASE,
                'headers' =>
                    [
                        'Authorization' => "Bearer {$access_token}"
                    ]
            ]
        );
    }

    /**
     * Get the access token from hootsuite to use with other requests
     *
     */
    public function getToken()
    {

        $client = new Client();

        try {

            $response = $client->post(
                self::ACCESS_TOKEN_URL, [
                'auth' => [
                    self::CLIENT_ID,
                    self::CLIENT_SECRET
                ],
                'form_params' => [
                    'grant_type' => self::GRANT_TYPE,
                    'member_id' => self::MEMBER_ID
                ]
            ]);

            $contents = Json::decode($response->getBody()->getContents());

            return $contents['access_token'];

        } catch (Exception $e) {

            LogToFile::error('Failed to get access token from Hootsuite. ' . $e->getMessage() . __METHOD__, 'submissions');

            // Failed to get the access token or user details.
            return false;
        }

    }

    /**
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    public function getScheduledPostTime(): string
    {
        $dateTime = new DateTime();

        $timeZone = new DateTimeZone( Craft::$app->getTimeZone() );
        $timeZoneOffset = $timeZone->getOffset($dateTime);

        // wait for media to upload
        $timeZoneOffset -= self::HOOTSUITE_POST_DELAY;

        $interval = DateInterval::createFromDateString((string)$timeZoneOffset . 'seconds');
        $dateTime->sub($interval);

        return $dateTime->format('Y-m-d\TH:i:s\Z');
    }

}
