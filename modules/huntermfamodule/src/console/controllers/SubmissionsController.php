<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule\console\controllers;

use modules\huntermfamodule\HuntermfaModule;

use Craft;
use putyourlightson\logtofile\LogToFile;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Submissions Commands
 *
 * The first line of this class docblock is displayed as the description
 * of the Console Command in ./craft help
 *
 * Craft can be invoked via commandline console by using the `./craft` command
 * from the project root.
 *
 * Console Commands are just controllers that are invoked to handle console
 * actions. The segment routing is module-name/controller-name/action-name
 *
 * The actionIndex() method is what is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft huntermfa-module/submissions
 *
 * Actions must be in 'kebab-case' so actionDoSomething() maps to 'do-something',
 * and would be invoked via:
 *
 * ./craft huntermfa-module/submissions/do-something
 *
 * @author    test
 * @package   TestModule
 * @since     1.0.0
 */
class SubmissionsController extends Controller
{
    // Public Methods
    // =========================================================================

    /**
     * Handle huntermfa-module/submissions console command
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function actionIndex()
    {
        echo "Test from the console SubmissionsController actionIndex() method\n";

        return true;
    }

    /**
     * Handle huntermfa-module/submissions/post console commands
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function actionPost()
    {
        echo "Executing console SubmissionsController->actionPost()\n";

        LogToFile::info('Initiating Cron job to post ready submissions to Hootsuite','submissions');

        return true;
    }
}
