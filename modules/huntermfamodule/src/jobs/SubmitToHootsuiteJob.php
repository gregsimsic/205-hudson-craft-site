<?php

namespace modules\huntermfamodule\jobs;

use craft\queue\BaseJob;
use modules\huntermfamodule\HuntermfaModule;
use putyourlightson\logtofile\LogToFile;

class SubmitToHootsuiteJob extends BaseJob
{
    public $submissionId;

    public function execute($queue)
    {

        HuntermfaModule::getInstance()->hootsuite->postToHootsuite($this->submissionId);

        LogToFile::info("Submission {$this->submissionId}: Executed queue job.",'submissions');

    }

    /**
     * @inheritdoc
     */
    protected function defaultDescription()
    {
        return "Posting submission ({$this->submissionId}) to Hootsuite";
    }

}