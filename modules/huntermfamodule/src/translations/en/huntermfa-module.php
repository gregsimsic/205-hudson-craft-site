<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

/**
 * huntermfa en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('huntermfa-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 */
return [
    'huntermfa plugin loaded' => 'huntermfa plugin loaded',
];
