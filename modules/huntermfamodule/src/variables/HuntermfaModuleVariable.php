<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule\variables;

use modules\huntermfamodule\HuntermfaModule;

use Craft;

/**
 * huntermfa Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.huntermfaModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 */
class HuntermfaModuleVariable
{
    // Public Methods
    // =========================================================================

    // this implements a technique to enable sub variables, accessed in Twig like so craft.huntermfa.submissions.method()
    // ref: https://craftcookbook.net/recipes/multiple-template-variables-in-plugin-development
    // note: it was necessary to run "composer dump-autoload -a" to get class recognized
    public function __call($name, $arguments)
    {
        $className = '\modules\huntermfamodule\variables\HuntermfaModule_' . ucfirst($name) . 'Variable';

        return (class_exists($className)) ? new $className() : null;
    }

    public function test()
    {
        return 'test in HuntermfaModuleVariable';
    }

    public function studentIsAlumni($student)
    {
        return HuntermfaModule::getInstance()->huntermfa->studentIsAlumni($student);
    }

    public function getCurrentSemesterCode()
    {
        return HuntermfaModule::getInstance()->huntermfa->getCurrentSemesterCode();
    }

    public function columnize($string)
    {
        return HuntermfaModule::getInstance()->huntermfa->columnize($string);
    }

    public function getPrecedingSemesterCode()
    {
        $year = date('Y');
        $semester = date('n') < 7 ? 1 : 2;

        return $year . '-' . $semester;
    }

    public function resolveSidebarContent($entry) {

        return HuntermfaModule::getInstance()->huntermfa->resolveSidebarContent($entry);
    }
}
