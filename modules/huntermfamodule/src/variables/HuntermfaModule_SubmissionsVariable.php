<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule\variables;

use modules\huntermfamodule\HuntermfaModule;

/**
 * submissions Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.huntermfaModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 */
class HuntermfaModule_SubmissionsVariable
{
    // Public Methods
    // =========================================================================

    public function test()
    {
        return 'test in SubmissionsVariable';
    }

    public function formatDescription($submission)
    {
        return HuntermfaModule::getInstance()->submissions->formatDescription($submission);
    }

    public function formatName($person)
    {
        return HuntermfaModule::getInstance()->submissions->formatName($person);
    }

}
