<?php
/**
 * huntermfa module for Craft CMS 3.x
 *
 * Hunter MFA site module
 *
 * @link      mfa205hudson.org
 * @copyright Copyright (c) 2020 Hunter MFA
 */

namespace modules\huntermfamodule;

use craft\base\Element;
use craft\elements\Entry;
use craft\events\ElementEvent;
use craft\events\RegisterElementActionsEvent;
use craft\helpers\DateTimeHelper;
use craft\helpers\ElementHelper;
use craft\mail\Message;
use craft\services\Elements;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use modules\huntermfamodule\actions\PostToHootsuite;
use modules\huntermfamodule\assetbundles\huntermfamodule\HuntermfaModuleAsset;
use modules\huntermfamodule\services\Huntermfa as HunterMfaService;
use modules\huntermfamodule\services\Hootsuite as HootsuiteService;
use modules\huntermfamodule\services\Submissions as SubmissionsService;
use modules\huntermfamodule\variables\HuntermfaModuleVariable;
use modules\huntermfamodule\fields\ReadOnly;
use modules\huntermfamodule\fields\SemesterDropdown as SemesterDropdownField;
use modules\huntermfamodule\fields\SEO as SEOField;

use Craft;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\TemplateEvent;
use craft\i18n\PhpMessageSource;
use craft\web\View;
use craft\web\UrlManager;
use craft\services\Fields;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use putyourlightson\logtofile\LogToFile;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Hunter MFA
 * @package   HuntermfaModule
 * @since     1.0.0
 *
 * @property  HunterMfaService $huntermfa
 * @property  HootsuiteService $hootsuite
 */
class HuntermfaModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * HuntermfaModule::$instance
     *
     * @var HuntermfaModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/huntermfamodule', $this->getBasePath());

        // Set the controllerNamespace based on whether this is a console or web request
        if (Craft::$app->getRequest()->getIsConsoleRequest()) {
            $this->controllerNamespace = 'modules\huntermfamodule\console\controllers';
        } else {
            $this->controllerNamespace = 'modules\huntermfamodule\controllers';
        }

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/huntermfamodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Base template directory
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $e) {
            if (is_dir($baseDir = $this->getBasePath().DIRECTORY_SEPARATOR.'templates')) {
                $e->roots[$this->id] = $baseDir;
            }
        });

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * HuntermfaModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        $this->setComponents([
            'huntermfa' => HunterMfaService::class,
            'hootsuite' => HootsuiteService::class,
            'submissions' => SubmissionsService::class
        ]);

        // Load our AssetBundle
        if (Craft::$app->getRequest()->getIsCpRequest()) {
            Event::on(
                View::class,
                View::EVENT_BEFORE_RENDER_TEMPLATE,
                function (TemplateEvent $event) {
                    try {
                        Craft::$app->getView()->registerAssetBundle(HuntermfaModuleAsset::class);
                    } catch (InvalidConfigException $e) {
                        Craft::error(
                            'Error registering AssetBundle - '.$e->getMessage(),
                            __METHOD__
                        );
                    }
                }
            );
        }

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['test'] = 'huntermfa/admin/test';
                $event->rules['updatecats'] = 'huntermfa/admin/update-categories';
            }
        );

        // Register our CP routes
//        Event::on(
//            UrlManager::class,
//            UrlManager::EVENT_REGISTER_CP_URL_RULES,
//            function (RegisterUrlRulesEvent $event) {
//                $event->rules['cpActionTrigger1'] = 'huntermfa-module/admin/do-something';
//            }
//        );

        // Register our fields
        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = SemesterDropdownField::class;
                $event->types[] = SEOField::class;
                $event->types[] = ReadOnly::class;
            }
        );

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('huntermfa', HuntermfaModuleVariable::class);
            }
        );

        // add 'Post to Hootsuite' element action to index gear menu
        Event::on(Entry::class,
            Element::EVENT_REGISTER_ACTIONS,
            function(RegisterElementActionsEvent $event) {
                if ($event->source === 'section:6f211ac6-0ffc-4159-b9d4-cdbcb556716c') {
                    $event->actions[] = PostToHootsuite::class;
                }
            }
        );

        // Alterations to Submissions on first save
        //   -- Expiry Date
        //   -- Ajax submitted dates for timezones
        Event::on(
            Elements::class,
            Elements::EVENT_BEFORE_SAVE_ELEMENT,
            function (ElementEvent $event) {
                $element = $event->element;

                if ($element instanceof \craft\elements\Entry
                    && $element->sectionId === '23'
                    && !ElementHelper::isDraftOrRevision($element))
                {
                    $submissionTypeId = $element->submissionType->all()[0]->id;

                    // set initial date and time values for form submissions
                    if ($event->isNew && !Craft::$app->request->isCpRequest) {

                        LogToFile::info("Submission '{$element->title}' received from frontend form. " . __METHOD__, 'submissions');

                        // adjust dates for timezone for exhibitions and events
                        if (in_array($submissionTypeId, [27303,27307], false)) {
                            $originalDate = Craft::$app->request->getBodyParam('fields')['exhibitionStartDate'];
                            $element->exhibitionStartDate = $this->adjustDateForTimeZone($originalDate);
                        }

                        // exhibitions
                        if ($submissionTypeId == 27303) {
                            $originalDate = Craft::$app->request->getBodyParam('fields')['exhibitionEndDate'];
                            $element->exhibitionEndDate = $this->adjustDateForTimeZone($originalDate);
                        }

                        // events
                        if ($submissionTypeId == 27307) {
                            $originalStartTime = Craft::$app->request->getBodyParam('fields')['startTime'];
                            $element->startTime = $this->adjustDateForTimeZone($originalStartTime);
                        }
                    }

                    // set initial status of submissions
                    if (!$element->hootsuiteStatus) {

                        $element->hootsuiteStatus = HootsuiteService::STATUS_UNSENT;

                    }

                    // set initial expiry date
                    if (!$element->expiryDate) {

                        // set expiry date...
                        switch($submissionTypeId) {

                            // Exhibitions
                            case 27303:
                                $element->expiryDate = DateTimeHelper::toDateTime( strtotime("+1 days", strtotime($element->exhibitionEndDate) ));
                                break;

                            // Events
                            case 27307:
                                // NOTE: add one day; note that perhaps adding in the UTC timezone difference may be better, assuming it would set it to midnight of the event day
                                $element->expiryDate = DateTimeHelper::toDateTime( strtotime("+1 days", strtotime($element->exhibitionStartDate) ));
                                break;

                            // Everything else
                            default:
                                // ... to 30 days from today--the day it is submitted
                                $expiryDate = DateTimeHelper::toDateTime(strtotime("+30 days"));
                                $element->expiryDate = $expiryDate;
                        }
                    }
                }
            }
        );

        // If submission status is 'Unsent', then schedule a post to hootsuite
        Event::on(
            Elements::class,
            Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (ElementEvent $event) {
                $element = $event->element;
                if ($element instanceof Entry
                    && $element->sectionId === '23'
                    && !ElementHelper::isDraftOrRevision($element)
                    && $element->enabled
                    && date('Y-m-d', $element->expiryDate->getTimestamp()) >= date('Y-m-d')
                    && $element->postToHootsuite)
                {

                    $status = $element->hootsuiteStatus;

                    if ($status === HootsuiteService::STATUS_UNSENT) {

                        $this->hootsuite->scheduleMessage($element);

                    }
                }
            }
        );

        // send notification email to admins
        Event::on(
            Elements::class,
            Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (ElementEvent $event) {
                $element = $event->element;
                if ($element instanceof Entry
                    && $element->sectionId === '23'
                    && !ElementHelper::isDraftOrRevision($element)
                    && $event->isNew)
                {

                    // must retrieve the submission from the database so that the date fields are normalized to DateTime objects
                    $submission = \craft\elements\Entry::find()
                        ->id($element->id)
                        ->anyStatus() // it will be disabled because that is the default submission status
                        ->with(['submissionsMedia','person','submissionType'])
                        ->one();

                    Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);
                    $body = Craft::$app->getView()->renderTemplate(
                        'huntermfa-module/_components/emails/Submission_Notification',
                        [
                            'submission' => $element,
                            'type' => $element->submissionType[0]->title,
                            'description' => self::$instance->submissions->formatDescription($submission),
                            'names' => self::$instance->submissions->formatName($submission->person),
                            'link' => $element->cpEditUrl
                        ]
                    );

                    $emailSettings = Craft::$app->getProjectConfig()->get('email');
                    $fromEmail = $emailSettings['fromEmail'];
                    $fromName = $emailSettings['fromName'];

                    $message = new Message();
                    $message->setFrom([$fromEmail => $fromName]);
                    $message->setTo([
                        'tlaun@hunter.cuny.edu',
                        'leslie.hunter.cuny@gmail.com',
                        'cfreeman.hunter@gmail.com',
                        'hunterartdeptnewsletter@gmail.com',
                        'greg@gregsimsic.com'
                    ]);
                    $message->setSubject('MFA Community Submission Received');
                    $message->setHtmlBody($body);

                    Craft::$app->mailer->send($message);

                }
            }
        );

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'huntermfa-module',
                '{name} module loaded',
                ['name' => 'huntermfa']
            ),
            __METHOD__
        );
    }

    /**
     * @param string date
     * @return string
     * @throws Exception
     */
    public function adjustDateForTimeZone($date) {
        $dateTime = new DateTime($date);
        $interval = $this->getTimezoneOffsetInterval($dateTime);
        $dateTime->sub($interval);
        return $dateTime->format('Y-m-d H:i:s');
    }

    /**
     * @param DateTime dateTime
     * @return DateInterval
     */
    public function getTimezoneOffsetInterval($dateTime) {
        $timeZone = new DateTimeZone( Craft::$app->getTimeZone() );
        $timeZoneOffset = $timeZone->getOffset($dateTime);
        return DateInterval::createFromDateString((string)$timeZoneOffset . 'seconds');
    }

    // Protected Methods
    // =========================================================================
}
