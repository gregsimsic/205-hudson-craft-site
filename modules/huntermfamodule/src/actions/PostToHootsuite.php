<?php

namespace modules\huntermfamodule\actions;

use Craft;
use craft\base\ElementAction;
use craft\elements\db\ElementQueryInterface;
use modules\huntermfamodule\HuntermfaModule;

/**
 *
 * @property string $triggerLabel
 * @property mixed $confirmationMessage
 */
class PostToHootsuite extends ElementAction {

    /**
     * @inheritdoc
     */
    public function getTriggerLabel(): string
    {
        return Craft::t('huntermfa-module', 'Post to Hootsuite');
    }

    /**
     * @inheritdoc
     */
    public function getConfirmationMessage() {
        return Craft::t('huntermfa-module', 'Are you sure? This action will send all selected submissions to Hootsuite whether or not they have already been sent.');
    }

    /**
     * @inheritdoc
     */
    public function performAction(ElementQueryInterface $query): bool
    {

        $count = 0;

        foreach ($query->all() as $submission) {
            if ($submission->enabled
                && $submission->postToHootsuite
                // do not send expired submissions
//                && date('Y-m-d', $submission->expiryDate->getTimestamp()) >= date('Y-m-d')
            ) {
                HuntermfaModule::getInstance()->hootsuite->scheduleMessage($submission);
                $count++;
            }
        }

        if ($count) {
            $this->setMessage(Craft::t('huntermfa-module', "Posted {$count} submission(s) to Hootsuite"));
        } else {
            $this->setMessage(Craft::t('huntermfa-module', 'No submissions were sent to Hootsuite'));
        }

        return true;
    }

}