/**
 * huntermfa module for Craft CMS
 *
 * huntermfa JS
 *
 * @author    Hunter MFA
 * @copyright Copyright (c) 2020 Hunter MFA
 * @link      mfa205hudson.org
 * @package   HuntermfaModule
 * @since     1.0.0
 */

// switch submission social media field label and instructions
const $socialMediaTab = $("#tab-social-media");

const $postToHootsuiteLabel = $("#fields-postToHootsuite-label");

const $instructions = $postToHootsuiteLabel.next('.instructions');

const statusValue = $("#fields-hootsuiteStatus-field input").val();

if (statusValue == 'Complete') {
    $postToHootsuiteLabel.text('Re-post to Hootsuite');
    $instructions.text('Turn this on to attempt to re-post this submission to Hootsuite.');
}
